<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'hal_pub_description' => 'Afficher les ressources bibliographiques HAL',
	'hal_pub_nom' => 'Publications HAL',
	'hal_pub_slogan' => 'Afficher les publications HAL',
);