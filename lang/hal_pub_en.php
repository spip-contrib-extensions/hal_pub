<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


	// B
	'but_rechercher' => 'Search',

	// C
	'cfg_titre_parametrages' => 'HAL Configuration',

	// E
	'explication_champs' => '(Optionnal)<br />Fields (coma separated list) to be returned by the HAL request<br />
	<br />If not specified, this list is used: title_s,authFullName_s,docid,label_xml,arxivId_s,halId_s,doiId_s,uri_s,docType_s,submittedDateY_i',
	'explication_types_document' => '(Optionnal)<br />Restrict to these document types (coma separated list)<br />Example: ART,OUV,DOUV,COUV<br />
	<br />If not specified, all types are returned',
	'explication_url_depot' => '(Optionnal)<br />Example:<br /><a href="https://api.archives-ouvertes.fr/search/CPT/">https://api.archives-ouvertes.fr/search/CPT/</a><br />
	<br />If not specified, the generic HAL URL is used <a href="https://api.archives-ouvertes.fr/search/">https://api.archives-ouvertes.fr/search/</a>',
	'explication_pagination' => 'Number of publications displayed per page.',

	// F
	'filtre_annee' => 'Year',
	'filtre_annee_intro' => 'All years',
	'filtre_pub' => 'Type of publication',
	'filtre_pub_intro' => 'All types',
	'filtre_equipe' => 'Team',
	'filtre_equipe_intro' => 'All teams',

	// H
	'hal_pub_titre' => 'HAL Publications',

	// L
	'label_champs' => 'Fields to be returned',
	'label_pagination' => 'Pagination',
	'label_types_document' => 'Document types',
	'label_url_depot' => 'URL of your HAL repository',

	// R
	'recherche_0' => 'No results',
	'recherche_1' => '1 resultat',
	'recherche_plus' => '@nb@ results',

	// T
	'titre_page_configurer_hal_pub' => 'HAL parameters',
	'type_pub_ART' => 'Article',
	'type_pub_COMM' => 'Proceedings',
	'type_pub_COUV' => 'Book section',
	'type_pub_DOUV' => 'Book direction',
	'type_pub_HDR' => 'HDR',
	'type_pub_LECTURE' => 'Lecture',
	'type_pub_MEM' => 'Student reports',
	'type_pub_OTHER' => 'Other',
	'type_pub_OUV' => 'Book',
	'type_pub_PATENT' => 'Patent',
	'type_pub_POSTER' => 'Poster',
	'type_pub_REPORT' => 'Report',
	'type_pub_THESE' => 'Thesis',
	'type_pub_UNDEFINED' => 'Preprint',
	'type_pub_VIDEO' => 'Vidoo',

);

