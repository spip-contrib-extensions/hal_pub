<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


	// B
	'but_rechercher' => 'Rechercher',

	// C
	'cfg_titre_parametrages' => 'Configuration HAL',

	// E
	'explication_champs' => '(Facultatif)<br />Liste des champs (séparés par une virgule) à retourner par la requête HAL<br />
	<br />Si ce champ n\'est pas renseigné, on utilise la liste: title_s,authFullName_s,docid,label_xml,arxivId_s,halId_s,doiId_s,uri_s,docType_s,submittedDateY_i',
	'explication_types_document' => '(Facultatif)<br />Restreindre les types de document (liste de types séparés par une virgule)<br />Exemple: ART,OUV,DOUV,COUV<br />
	<br />Si ce champ n\'est pas renseigné, tous les types de document sont retournés',
	'explication_url_depot' => '(Facultatif)<br />Exemple:<br /><a href="https://api.archives-ouvertes.fr/search/CPT/">https://api.archives-ouvertes.fr/search/CPT/</a><br />
	<br />Si ce champ n\'est pas renseigné, on utilise l\'URL générale	de HAL <a href="https://api.archives-ouvertes.fr/search/">https://api.archives-ouvertes.fr/search/</a>',
	'explication_pagination' => 'Nombre de publications affichées par page.',

	// F
	'filtre_annee' => 'Année',
	'filtre_annee_intro' => 'Toutes les années',
	'filtre_pub' => 'Type de publication',
	'filtre_pub_intro' => 'Tous les types',
	'filtre_equipe' => 'Équipe',
	'filtre_equipe_intro' => 'Toutes les équipes',

	// H
	'hal_pub_titre' => 'Publications HAL',

	// L
	'label_champs' => 'Champs à retourner',
	'label_pagination' => 'Pagination',
	'label_types_document' => 'Types de document',
	'label_url_depot' => 'URL de votre dépot&nbsp;HAL',

	// R
	'recherche_0' => 'Aucun résultat',
	'recherche_1' => '1 résultat',
	'recherche_plus' => '@nb@ résultats',

	// T
	'titre_page_configurer_hal_pub' => 'Paramètres HAL',
	'type_pub_ART' => 'Article dans une revue',
	'type_pub_COMM' => 'Communication dans un congrès',
	'type_pub_COUV' => 'Chapitre d\'ouvrage',
	'type_pub_DOUV' => 'Direction d\'ouvrage, Proceedings',
	'type_pub_HDR' => 'HDR',
	'type_pub_LECTURE' => 'Cours',
	'type_pub_MEM' => 'Mémoire d\'étudiant',
	'type_pub_OTHER' => 'Autre publication',
	'type_pub_OUV' => 'Ouvrage (y compris édition critique et traduction)',
	'type_pub_PATENT' => 'Brevet',
	'type_pub_POSTER' => 'Poster',
	'type_pub_REPORT' => 'Rapport',
	'type_pub_THESE' => 'Thèse',
	'type_pub_UNDEFINED' => 'Pré-publication, Document de travail',
	'type_pub_VIDEO' => 'Vidéo',

);

