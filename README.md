# Plugin HAL Pub

![HAL](./prive/themes/spip/images/hal_pub-xx.svg)

Le plugin HAL Pub permet d'afficher les publications du portail [HAL](https://hal.science/) soit globalement soit pour un laboratoire donné.

## Documentation
https://contrib.spip.net/4839
